﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnAir2
{
    public partial class _default : System.Web.UI.Page
    {
        public const string scheduleEnding = "2:00AM";
        public const string midnight = "12:00AM";

        string startTime = "";
        string endTime = "";
        string day = "";
        string who = "";
        string desc = "";
        string jobTitle = "";
        string wzndimage = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            readXML();

            StartTime1.Text = startTime;
            EndTime1.Text = endTime;
            DJWho.Text = who;
            JobTitle1.Text = jobTitle;
            DJImage.Attributes["src"] = "http://communication.illinoisstate.edu/" + wzndimage;
        }
        private void readXML()
        {
            try
            {
                System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
                xDoc.Load("http://communication.illinoisstate.edu/files/scripts/wzndschedule.xml");

                foreach (System.Xml.XmlNode node in xDoc.DocumentElement.ChildNodes)
                {
                    startTime = node.SelectSingleNode("starttime").InnerText.Trim();
                    endTime = node.SelectSingleNode("endtime").InnerText.Trim();
                    day = node.SelectSingleNode("day").InnerText.Trim();//string text = node.InnerText; //or loop through its children as well
                    if (CheckTime(startTime, endTime, day))
                    {
                        jobTitle = node.SelectSingleNode("jobtitle").InnerText.Trim();
                        who = node.SelectSingleNode("who").InnerText.Trim();
                        desc = node.SelectSingleNode("description").InnerText.Trim();
                        wzndimage = node.SelectSingleNode("image").InnerText.Trim();
                        break;
                    }

                }
            }
            catch (System.Net.WebException web)
            {
                who = "There was an error grabbing this feed. If this error continues to occur, please contact CAS-IT TAP staff. (ErrorC465)";
                startTime = " ";
                endTime = " ";
            }
            catch (System.NullReferenceException nul)
            {
                who = "There was an error grabbing this feed. If this error continues to occur, please contact CAS-IT TAP staff. (ErrorC465)";
                startTime = " ";
                endTime = " ";
            }
            catch (System.TimeoutException time)
            {
                who = "There was an error grabbing this feed. If this error continues to occur, please contact CAS-IT TAP staff. (ErrorC465)";
                startTime = "";
                endTime = "";
            }
        }//end readXML
        private bool CheckTime(string StartTime, string EndTime, string StartDay)
        {
            System.DateTime central = System.TimeZoneInfo.ConvertTimeBySystemTimeZoneId(
            System.DateTime.UtcNow, "Central Standard Time");

            System.DateTime start = Convert.ToDateTime(StartTime);
            System.DateTime end = Convert.ToDateTime(EndTime);
            System.DateTime Midnight = Convert.ToDateTime(midnight);
            System.DateTime Ending = Convert.ToDateTime(scheduleEnding);


            if (StartTime.Contains("PM") && EndTime.Contains("AM"))
            {
                Ending = Convert.ToDateTime(EndTime);
                EndTime = System.DateTime.Now.AddDays(1).ToString("MM/dd/yyyy") + " " + EndTime;
                end = Convert.ToDateTime(EndTime);
            }
            string dayofweek = System.DateTime.Now.DayOfWeek.ToString();
            if (Midnight <= central && central <= Ending)
            {
                dayofweek = System.DateTime.Now.AddDays(-1).DayOfWeek.ToString();
                if (StartTime.Contains("PM") && EndTime.Contains("AM"))
                {
                    StartTime = System.DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " " + StartTime;
                    start = Convert.ToDateTime(StartTime);
                }
            }

            if (dayofweek == StartDay)
            {

                if (start > central || central > end)
                {
                    return false;
                }
                return true;
            }
            else
                return false;
        }//end checkTime
    }
}